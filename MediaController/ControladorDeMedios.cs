﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace MediaController
{
    public partial class ControladorDeMedios : Form
    {
        private string bufferdeEntrada;
        // Get a handle to an application window.
        [DllImport("USER32.DLL", CharSet = CharSet.Unicode)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        // Activate an application window.
        [DllImport("USER32.DLL")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);
        [DllImport("user32.dll", SetLastError = true)]
        static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, UIntPtr dwExtraInfo);
        private delegate void delegadoAcceso(string accion);
        public ControladorDeMedios()
        {
            InitializeComponent();
        }
        //Metodos para que el puerto serial tenga acceso al form
        private void AccesoForm(string accion)
        {
            bufferdeEntrada = accion;
            TextDatoRecibido.Text = bufferdeEntrada;
            eventosControl(Int32.Parse(bufferdeEntrada));           
        }

        private void accesoInterrupcion(string accion)
        {
            delegadoAcceso var_delegadoAccesso;
            var_delegadoAccesso = new delegadoAcceso(AccesoForm);
            object[] arg = { accion };
            base.Invoke(var_delegadoAccesso, arg);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            bufferdeEntrada = "";
            btnConectar.Enabled = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        } 
        private void KeyEvent(byte ev)
        {
            keybd_event(ev, 0, 0, UIntPtr.Zero);
            keybd_event(ev, 0, 2, UIntPtr.Zero);            
        }
        private void button3_Click(object sender, EventArgs e)
        {
            
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            if (this.WindowState==FormWindowState.Minimized)
            {
                this.Hide();
                segundoPlano.Icon = SystemIcons.Application;
                segundoPlano.BalloonTipText = "Media Controller";
                segundoPlano.ShowBalloonTip(2000);
            }
        }

        private void segundoPlano_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
        }

        private void restaurarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Show();            
        }

        private void minimizarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();            
            segundoPlano.BalloonTipText = "Media Controller";
            segundoPlano.ShowBalloonTip(1000);            
        }

        private void cerrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBuscarPuertos_Click(object sender, EventArgs e)
        {
            comboPuertos.Items.Clear();
            string[] puertosDisponibles = SerialPort.GetPortNames();
            foreach (string puerto in puertosDisponibles)
            {
                comboPuertos.Items.Add(puerto);
            }
            if (comboPuertos.Items.Count > 0)
            {
                MessageBox.Show("Seleccionar Un puerto");
                btnConectar.Enabled = true;
            }
            else
            {
                MessageBox.Show("No se encontro un puerto Detectado");
                comboPuertos.Items.Clear();
                comboPuertos.Text = "";
                bufferdeEntrada = "";
            }
        }

        private void btnConectar_Click(object sender, EventArgs e)
        {
            try
            {
                if (btnConectar.Text == "Conectar")
                {
                    serialPuerto.BaudRate = Int32.Parse("9600");
                    serialPuerto.DataBits = 8;
                    serialPuerto.Parity = Parity.None;
                    serialPuerto.StopBits = StopBits.One;
                    serialPuerto.Handshake = Handshake.None;
                    serialPuerto.PortName = comboPuertos.Text;
                    try
                    {
                        serialPuerto.Open();
                        btnConectar.Text = "Desconectar";
                        MessageBox.Show("Conectado");
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show(exc.Message.ToString());
                    }
                }
                else if (btnConectar.Text == "Desconectar")
                {
                    serialPuerto.Close();
                    btnConectar.Text = "Conectar";
                    btnConectar.Enabled = false;
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message.ToString());
            }
        }
        private void datoRecibido(object sender, SerialDataReceivedEventArgs e)
        {
            accesoInterrupcion(serialPuerto.ReadLine());
        }

        private void eventosControl(int op)
        {
            switch (op)
            {
                //Boton Power-------------------------------------------
                case 0:
                    break;
                //Boton return------------------------------------------
                case 1:
                    break;
                //boton Mode-------------------------------------------
                case 2:
                    break;
                //boton Chanel -
                case 3:
                    KeyEvent(0x25);
                    break;                
                //boton Chanel +
                case 4:
                    KeyEvent(0x27);
                    break;                
              //boton prev track
                case 6:
                    KeyEvent(0xB1);
                    break;
                //boton next track
                case 7:
                    KeyEvent(0xB0);
                    break;
                //boton play-pause
                case 8:
                    KeyEvent(0xB3);
                    break;
                //boton volume -
                case 9:
                    KeyEvent(0xAE);
                    break;
                //boton volume +
                case 10:
                    KeyEvent(0xAF);
                    break;
            }
        }
    }   
}
