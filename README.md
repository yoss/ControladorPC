Este proyecto fue hecho con la finalidad de controlar algunas funciones basicas de la computadora por medio de un control remoto,
el cual consta de una aplicación hecha en c#, dicho proyecto consta en que un arduino recibe las pulsaciones por infrarrojo y este envia los datos por medio
del puerto serial, y la aplicacion .exe activa funciones de la computadora de acuerdo a los datos recibidos como es:

* subir volumen
* bajar volumen
* siguiente cancion
* cancion anterior
* play 
* pause
* stop

y se puede configurar muchas funciones más...
